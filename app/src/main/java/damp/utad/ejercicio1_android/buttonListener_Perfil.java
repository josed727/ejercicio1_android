package damp.utad.ejercicio1_android;

/**
 * Created by juan.marquerie on 20/10/2015.
 */

import android.view.View;
import android.view.View.OnClickListener;

/**
 * Created by juan.marquerie on 16/10/2015.
 */
public class buttonListener_Perfil implements OnClickListener{
    private Perfil per;

    public buttonListener_Perfil(Perfil per){

        this.per=per;
    }

    public void onClick(View v) {
        if(v.getId()==R.id.buttonEdit)
        {
            per.editTap();
        }
        else if(v.getId()== R.id.buttonVol)
        {
            per.volTap();
        }

    }
}
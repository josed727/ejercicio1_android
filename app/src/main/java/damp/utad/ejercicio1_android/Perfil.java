package damp.utad.ejercicio1_android;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Perfil extends AppCompatActivity
{

    private Button bEdit;
    private Button bVolver;
    private EditText edNombre;
    private EditText edEmail;
    private EditText edTelefono;
    private EditText edDireccion;
    private String nombre;
    private String email;
    private String direccion;
    private long telefono;
    private boolean edit;
    private buttonListener_Perfil btnLP;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        String[] datos=null;
        //datos==MainActivity.getStrDatos();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);
        btnLP=new buttonListener_Perfil(this);
        nombre=datos[0];
        email=datos[1];
        direccion=datos[2];
        try {
            telefono = Long.parseLong(datos[3]);
        }catch (NumberFormatException ex)
        {}
        edit=false;
        confBottones();
        confEditText();
    }

    public void setView()
    {
        setContentView(R.layout.activity_perfil);
    }

    public void confBottones()
    {
        bEdit=(Button)findViewById(R.id.buttonEdit);
        bEdit.setOnClickListener(btnLP);
        bVolver=(Button)findViewById(R.id.buttonVol);
        bVolver.setOnClickListener(btnLP);
    }

    public void confEditText()
    {
        edNombre=(EditText)findViewById(R.id.eTnombre);
        edEmail=(EditText)findViewById(R.id.edMail);
        edTelefono=(EditText)findViewById(R.id.edTel);
        edDireccion=(EditText)findViewById(R.id.edDir);
    }

    public void editTap()
    {
        if(edit)
        {
            String[] datos=new String[4];
            datos[0]=edNombre.getText().toString();
            datos[1]=edEmail.getText().toString();
            datos[2]=edDireccion.getText().toString();
            datos[3]= edTelefono.getText().toString();
            //MainActivity.setDatos(datos);
        }
        else
        {
            edit=true;
            bEdit.setText("GUARDAR");
            edNombre.setEnabled(true);
            edEmail.setEnabled(true);
            edTelefono.setEnabled(true);
            edDireccion.setEnabled(true);
        }
    }

    public void volTap()
    {
        this.finish();
    }
}
